import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homeet/models/event.dart';
import 'package:homeet/models/models.dart';
import 'package:homeet/services/event_service.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/createEvent_view.dart';
import 'package:homeet/widgets/event_widget.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EventsView extends StatelessWidget {
  final Group group;
  const EventsView({Key? key, required this.group}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final eventService = Provider.of<EventService>(context);

    Widget getEventWigets(List<String>? strings, EventService evService) {
      if (strings == null || strings.isEmpty) {
        return const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "The group dones not have events planned yet",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
        );
      }

      return Column(
        children: strings
            .map((item) => FutureBuilder(
                future: evService.getEventByName(item),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CircularProgressIndicator(
                        color: AppTheme.primary,
                      ),
                    );
                  }
                  if (snapshot.data == null) {
                    return const Text(
                        "There was an error loading the event info");
                  } else {
                    Event ev = snapshot.data as Event;
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: EventCard(event: ev),
                        )
                      ],
                    );
                  }
                }))
            .toList(),
      );
    }

    final createEvent_button = InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreateEventView(
                      group: group,
                    )));
      },
      child: Container(
        margin: EdgeInsets.only(top: 10.0, bottom: 20),
        height: 45.0,
        width: 200.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(30),
            ),
            color: AppTheme.primary),
        child: Center(
          child: Text(
            "Create Event",
            style: TextStyle(
                fontSize: 18.0, fontFamily: "Lato", color: Colors.white),
          ),
        ),
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text("Events"),
        ),
        backgroundColor: Colors.white,
        body: Column(children: [
          getEventWigets(group.events, eventService),
          createEvent_button
        ]));
  }
}
