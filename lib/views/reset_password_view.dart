import 'package:flutter/material.dart';
import 'package:homeet/models/models.dart';
import 'package:homeet/providers/providers.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/themes.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';

class ResetPasswordScreen extends StatelessWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppTheme.primary,
        body: Center(
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const Image(
                    image: AssetImage("assets/logo/logo-yellow.png"),
                    width: 250,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text(
                    "Reset your password",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  ChangeNotifierProvider(
                    create: (_) => LoginFormProvider(),
                    child: const _LoginForm(),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class _LoginForm extends StatelessWidget {
  const _LoginForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);
    return Form(
        key: loginForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Email",
                ),
                onChanged: (value) => loginForm.email = value,
                validator: (value) {
                  String pattern =
                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regExp = RegExp(pattern);
                  return regExp.hasMatch(value ?? "") ? null : "Invalid email";
                },
              ),
              const SizedBox(
                height: 20,
              ),
              MaterialButton(
                onPressed: loginForm.isLoading
                    ? null
                    : () async {
                        bool hasInternet =
                            await InternetConnectionChecker().hasConnection;
                        if (hasInternet) {
                          FocusScope.of(context).unfocus();
                          final authService =
                              Provider.of<AuthService>(context, listen: false);
                          authService.resetPassword(loginForm.email);
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text("Email Sent"),
                              content: const Text(
                                  "An email to reset your password has been sent your email address, check it and come back"),
                              actions: [
                                TextButton(
                                    onPressed: () =>
                                        Navigator.pushReplacementNamed(
                                            context, "login"),
                                    child: const Text(
                                      "Ok",
                                      style: TextStyle(color: AppTheme.primary),
                                    ))
                              ],
                            ),
                          );
                        } else {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text("No internet"),
                              content: const Text(
                                  "There is no internet connection, check for a network and try again"),
                              actions: [
                                TextButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: const Text(
                                      "Ok",
                                      style: TextStyle(color: AppTheme.primary),
                                    ))
                              ],
                            ),
                          );
                        }
                      },
                disabledColor: Colors.grey,
                elevation: 0,
                color: AppTheme.secondary,
                child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Text(
                      loginForm.isLoading ? "Loading" : "Send reset email",
                      style: const TextStyle(
                          color: AppTheme.primary, fontSize: 18),
                      textAlign: TextAlign.center,
                    )),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ],
          ),
        ));
  }
}
