import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:homeet/providers/createEvent_form_provider.dart';
import 'package:homeet/services/event_service.dart';
import 'package:homeet/services/group_service.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/themes/event_create_decoration.dart';
import 'package:homeet/themes/input_login_decoration.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../models/models.dart';

class CreateEventView extends StatelessWidget {
  final Group group;

  const CreateEventView({Key? key, required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final eventService = Provider.of<EventService>(context);
    // TODO: implement build
    return ChangeNotifierProvider(
      create: (_) => EventCreateformProvider(),
      child: _FormBody(
        eventService: eventService,
        group: group,
      ),
    );
    ;
  }
}

class _FormBody extends StatelessWidget {
  final Group group;
  const _FormBody({Key? key, required this.eventService, required this.group})
      : super(key: key);

  final EventService eventService;

  @override
  Widget build(BuildContext context) {
    final editForm = Provider.of<EventCreateformProvider>(context);
    final groupService = Provider.of<GroupService>(context);

    bool seCreo = false;

    String generateRandomId() {
      var uuid = Uuid();
      return uuid.v1();
    }

    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Create event"),
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: editForm.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    initialValue: editForm.name,
                    autocorrect: false,
                    decoration: InputEventDecorations.authInputDecoration(
                      labelText: "Event Name",
                    ),
                    onChanged: (value) => {editForm.updateName(value)},
                    validator: (value) {
                      if (value != null && value != "") return null;
                      return "This field is necessary";
                    },
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(30),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    initialValue: editForm.name,
                    autocorrect: false,
                    decoration: InputEventDecorations.authInputDecoration(
                      labelText: "Location",
                    ),
                    onChanged: (value) => {editForm.updateLocation(value)},
                    validator: (value) {
                      if (value != null && value != "") return null;
                      return "This field is necessary";
                    },
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(30),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    initialValue: editForm.name,
                    autocorrect: false,
                    decoration: InputEventDecorations.authInputDecoration(
                      labelText: "Price",
                    ),
                    onChanged: (value) => {editForm.updatePrice(value)},
                    validator: (value) {
                      if (value != null && value != "") return null;
                      return "This field is necessary";
                    },
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(30),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          Color.fromARGB(255, 33, 194, 147),
                        ),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ))),
                    onPressed: () async {
                      DateTime? selectedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime(2000),
                          firstDate: DateTime(1900),
                          lastDate: DateTime(2025),
                          helpText: "SELECT YOUR BIRTH DAY",
                          builder: (context, child) => Theme(
                              data: ThemeData().copyWith(
                                  colorScheme: const ColorScheme.light(
                                primary: AppTheme.primary,
                              )),
                              child: child!));
                      if (selectedDate != null) {
                        editForm.updateDate(selectedDate);
                      }
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Event date - ${editForm.date.year}/${editForm.date.month}/${editForm.date.day}",
                        style:
                            const TextStyle(color: Colors.white, fontSize: 17),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    seCreo = true;
                    String id = generateRandomId();
                    editForm.updateId(id);
                    eventService.createEvent(editForm.name, editForm.date,
                        editForm.location, double.parse(editForm.price), id);
                    groupService.addEvent(editForm.id, group);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 15.0),
                    height: 35.0,
                    width: 130.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(30),
                        ),
                        color: AppTheme.primary),
                    child: Center(
                      child: Text(
                        "Create!",
                        style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: "Lato",
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
                (seCreo) ? Text("se creooo") : Text(""),
              ],
            ),
          ),
        ));
  }
}
