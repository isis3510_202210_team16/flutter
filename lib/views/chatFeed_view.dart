import 'package:flutter/material.dart';
import 'package:homeet/themes/app_theme.dart';

class ChatFeedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 20, bottom: 20),
      alignment: Alignment.bottomRight,
      child: FloatingActionButton(
        onPressed: () {
          print("hellloooo");
        },
        backgroundColor: AppTheme.primary,
        child: const Icon(Icons.search),
      ),
    );
  }
}
