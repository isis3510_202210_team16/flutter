import 'package:flutter/material.dart';
import 'package:homeet/models/models.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/app_icons.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:provider/provider.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../providers/login_form_provider.dart';
import '../themes/input_login_decoration.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppTheme.primary,
        body: Center(
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const Image(
                    image: AssetImage("assets/logo/logo-yellow.png"),
                    width: 250,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text(
                    "Login to your account",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  ChangeNotifierProvider(
                    create: (_) => LoginFormProvider(),
                    child: const _LoginForm(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextButton(
                    onPressed: () => Navigator.pushReplacementNamed(
                        context, "reserPassword"),
                    child: const Text("Forgot your password?"),
                    style: TextButton.styleFrom(primary: Colors.white),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextButton(
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, "register"),
                    child: const Text("Don't have an account? Sign up"),
                    style: TextButton.styleFrom(primary: Colors.white),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class _LoginForm extends StatelessWidget {
  const _LoginForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);
    return Form(
        key: loginForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Email",
                ),
                onChanged: (value) => loginForm.email = value,
                validator: (value) {
                  String pattern =
                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regExp = RegExp(pattern);
                  return regExp.hasMatch(value ?? "") ? null : "Invalid email";
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                autocorrect: false,
                obscureText: true,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Password",
                ),
                onChanged: (value) => loginForm.password = value,
                validator: (value) {
                  if (value != null && value.length >= 6) return null;
                  return "Pssword should be at least 6 characters long";
                },
              ),
              const SizedBox(
                height: 20,
              ),
              MaterialButton(
                onPressed: loginForm.isLoading
                    ? null
                    : () async {
                        bool hasInternet =
                            await InternetConnectionChecker().hasConnection;
                        if (hasInternet) {
                          FocusScope.of(context).unfocus();
                          final authService =
                              Provider.of<AuthService>(context, listen: false);

                          final userService =
                              Provider.of<UserService>(context, listen: false);

                          if (!loginForm.isValidForm()) return;

                          loginForm.isLoading = true;

                          final String? errorMessage = await authService.login(
                              loginForm.email, loginForm.password);

                          if (errorMessage == null) {
                            User? user = await userService
                                .getUser(loginForm.email.split(".")[0]);
                            if (user == null) {
                              NotificationsService.showSnackBar(
                                  "There was an error loading user data");
                              loginForm.isLoading = false;
                            } else if (!user.hasData) {
                              Navigator.pushReplacementNamed(
                                  context, "editProfile");
                            } else {
                              Navigator.pushReplacementNamed(
                                  context, "profile");
                            }
                          } else {
                            NotificationsService.showSnackBar(
                                "Invalid credentials");
                            loginForm.isLoading = false;
                          }
                        } else {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text("No internet"),
                              content: const Text(
                                  "There is no internet connection, check for a network and try again"),
                              actions: [
                                TextButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: const Text(
                                      "Ok",
                                      style: TextStyle(color: AppTheme.primary),
                                    ))
                              ],
                            ),
                          );
                        }
                      },
                disabledColor: Colors.grey,
                elevation: 0,
                color: AppTheme.secondary,
                child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Text(
                      loginForm.isLoading ? "Loading" : "Log In",
                      style: const TextStyle(
                          color: AppTheme.primary, fontSize: 18),
                      textAlign: TextAlign.center,
                    )),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ],
          ),
        ));
  }
}
