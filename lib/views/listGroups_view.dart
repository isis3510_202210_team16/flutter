import 'package:flutter/widgets.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homeet/models/friend.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/groupDetail_view.dart';
import 'package:homeet/widgets/profile_group.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../models/models.dart';
import '../services/services.dart';

class ListGroups extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final groupService = Provider.of<GroupService>(context);
    groupService.checkConnection();
    if (!groupService.hasConnection) {
      return Center(
          child: Text(
        "There is no internet connection",
        style: GoogleFonts.inter(fontSize: 30, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ));
    }
    return FutureBuilder(
      future: groupService.getAllGroups(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: CircularProgressIndicator(
              color: AppTheme.primary,
            ),
          );
        }
        if (snapshot.data == null) {
          return const Text("There was an error loading the info group");
        } else {
          List<Group> grupos = snapshot.data as List<Group>;
          return Column(
              children: grupos
                  .map(
                    (grupo) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: GestureDetector(
                          onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    GroupDetail(group: grupo)),
                          ),
                          child: ProfileGroup(group: grupo),
                        )),
                  )
                  .toList());
        }
      },
    );
  }
}
