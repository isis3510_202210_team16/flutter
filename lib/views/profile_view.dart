import 'package:flutter/material.dart';
import 'package:homeet/services/auth_service.dart';

import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/chatFeed_view.dart';

import 'package:homeet/views/find_view.dart';

import 'package:homeet/views/login_view.dart';
import 'package:homeet/views/premium_view.dart';
import 'package:homeet/widgets/profile_info.dart';
import 'package:homeet/widgets/users_list_page.dart';
import 'package:homeet/widgets/users_map.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfileView();
  }
}

// ignore: non_constant_identifier_names
class _ProfileView extends State<ProfileView> {
  int indexTap = 0;
  final List<Widget> widgetsChildren = [
    ProfileInfo(),
    FindView(),
    MapScreen(),
    PremiumView(),
    ChatFeedView(),
  ];
  var nombres = ["Profile", "Find", "Map", "Purchase", "Chat"];

  void onTapTapped(int index) {
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    String title = nombres[indexTap];

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: (title == "Profile")
            ? IconButton(
                icon: const Icon(Icons.logout_outlined),
                onPressed: () {
                  authService.logout();
                  Navigator.pushReplacementNamed(context, "login");
                },
              )
            : null,
      ),
      backgroundColor: Colors.white,
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
            primaryColor: Colors.black,
          ),
          child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              backgroundColor: AppTheme.primary,
              onTap: onTapTapped,
              currentIndex: indexTap,
              items: [
                BottomNavigationBarItem(
                    icon: Icon(Icons.person),
                    label: "Profile",
                    backgroundColor: AppTheme.primary),
                BottomNavigationBarItem(
                    icon: Icon(Icons.search),
                    label: "Find",
                    backgroundColor: AppTheme.primary),
                BottomNavigationBarItem(
                    icon: Icon(Icons.map),
                    label: "Map",
                    backgroundColor: AppTheme.primary),
                BottomNavigationBarItem(
                    icon: Icon(Icons.payment),
                    label: "Purchase",
                    backgroundColor: AppTheme.primary),
                BottomNavigationBarItem(
                    icon: Icon(Icons.chat),
                    label: "Chat",
                    backgroundColor: AppTheme.primary),
              ])),
    );
  }
}
