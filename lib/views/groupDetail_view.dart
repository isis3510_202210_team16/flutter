import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:homeet/models/group.dart';
import 'package:homeet/services/user_service.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/event_view.dart';
import 'package:provider/provider.dart';

import '../services/services.dart';

class GroupDetail extends StatelessWidget {
  final Group group;
  const GroupDetail({Key? key, required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final groupService = Provider.of<GroupService>(context);
    final userService = Provider.of<UserService>(context);

    String? username = userService.loggedUser.username;

    final cont_image = Container(
      height: 350,
      child: Image(image: getGroupImage(group)),
    );

    final title_stars = Container(
        margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  group.name.toUpperCase(),
                  style: TextStyle(
                      fontFamily: "Monserrat",
                      fontSize: 40.0,
                      fontWeight: FontWeight.w900),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "${group.members} members",
                  style: TextStyle(
                      fontFamily: "Monserrat",
                      fontSize: 25.0,
                      fontWeight: FontWeight.w900),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ],
        ));

    final description = Container(
      margin: new EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "Group description",
                style: TextStyle(
                    fontFamily: "Monserrat",
                    fontSize: 30.0,
                    fontWeight: FontWeight.w900),
                textAlign: TextAlign.left,
              ),
            ],
          ),
          Text(
            group.description,
            style: const TextStyle(
                fontFamily: "Lato",
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFF56575a)),
          ),
        ],
      ),
    );

    final privateGroup_events = Container(
        margin: new EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.lock,
              color: Color.fromARGB(255, 0, 0, 0),
              size: 24.0,
            ),
            Text(
              "Private group",
              style: const TextStyle(
                  fontFamily: "Lato",
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF56575a)),
            ),
            InkWell(
              onTap: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EventsView(group: group)))
              },
              child: Container(
                margin: EdgeInsets.only(top: 15.0, left: 100.0),
                height: 35.0,
                width: 130.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                    color: AppTheme.primary),
                child: Center(
                  child: Text(
                    "Events",
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: "Lato",
                        color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        ));

    final join_button = InkWell(
      onTap: () {
        groupService.joinGroup(group, username!);
        userService.addGroup(group);
      },
      child: Container(
        margin: EdgeInsets.only(top: 35.0, bottom: 20),
        height: 45.0,
        width: 130.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(30),
            ),
            color: AppTheme.primary),
        child: Center(
          child: Text(
            "Join",
            style: TextStyle(
                fontSize: 18.0, fontFamily: "Lato", color: Colors.white),
          ),
        ),
      ),
    );

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            cont_image,
            title_stars,
            privateGroup_events,
            description,
            join_button
          ],
        ),
      ),
    );
  }

  ImageProvider getGroupImage(Group group) {
    if (group.image == "") {
      return const AssetImage("assets/images/missing-avatar.png");
    } else if (group.image.startsWith("http")) {
      return CachedNetworkImageProvider(
        group.image,
      );
    }
    return FileImage(File(group.image));
  }
}
