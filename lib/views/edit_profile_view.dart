import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:homeet/providers/edit_profile_provider.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/themes.dart';

class EditProfileView extends StatelessWidget {
  const EditProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context);
    return ChangeNotifierProvider(
      create: (_) => EditProfileProvider(userService.loggedUser),
      child: _EditProfileBody(
        userService: userService,
      ),
    );
  }
}

class _EditProfileBody extends StatelessWidget {
  const _EditProfileBody({Key? key, required this.userService})
      : super(key: key);

  final UserService userService;

  @override
  Widget build(BuildContext context) {
    final editForm = Provider.of<EditProfileProvider>(context);
    return Scaffold(
      backgroundColor: AppTheme.primary,
      body: Center(
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Stack(children: [
                  GestureDetector(
                    onTap: () async {
                      final picker = ImagePicker();
                      final PickedFile? pickedFile = await picker.getImage(
                          source: ImageSource.camera,
                          imageQuality: 100,
                          preferredCameraDevice: CameraDevice.rear);
                      if (pickedFile == null) {
                        NotificationsService.showSnackBar(
                            "Taking profile picture canceled");
                        return;
                      }
                      userService.updateAvatarImage(pickedFile.path);
                    },
                    child: CircleAvatar(
                      maxRadius: 100,
                      backgroundImage: getImage(userService.loggedUser.avatar),
                    ),
                  ),
                  Positioned(
                      right: 0,
                      bottom: 0,
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: AppTheme.secondary),
                        child: IconButton(
                          icon: const Icon(
                            Icons.camera_alt_outlined,
                            color: AppTheme.primary,
                          ),
                          onPressed: () async {
                            final picker = ImagePicker();
                            final PickedFile? pickedFile =
                                await picker.getImage(
                                    source: ImageSource.camera,
                                    imageQuality: 100,
                                    preferredCameraDevice: CameraDevice.rear);
                            if (pickedFile == null) {
                              NotificationsService.showSnackBar(
                                  "Taking profile picture canceled");
                              return;
                            }
                            userService.updateAvatarImage(pickedFile.path);
                          },
                          iconSize: 40,
                        ),
                      )),
                ]),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Tell us about yourself!",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
                const SizedBox(
                  height: 20,
                ),
                const _EditForm(),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: userService.isLoading
            ? const CircularProgressIndicator(
                color: Colors.white,
              )
            : const Icon(
                Icons.save_outlined,
                size: 40,
                color: AppTheme.primary,
              ),
        onPressed: userService.isLoading
            ? null
            : () async {
                if (!editForm.isValidForm()) return;
                editForm.user.hasData = true;
                userService
                    .saveUserProfile(editForm.user)
                    .then((_) => userService.uploadAvatarImage());
                Navigator.pushReplacementNamed(context, "profile");
              },
        backgroundColor: AppTheme.secondary,
      ),
    );
  }

  ImageProvider getImage(String avatarImage) {
    if (avatarImage == "") {
      return const AssetImage("assets/images/missing-avatar.png");
    } else if (avatarImage.startsWith("http")) {
      return CachedNetworkImageProvider(
        avatarImage,
      );
    }
    return FileImage(File(avatarImage));
  }
}

class _EditForm extends StatelessWidget {
  const _EditForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final editForm = Provider.of<EditProfileProvider>(context);
    final user = editForm.user;
    String selectedValue = user.gender;
    return Form(
        key: editForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                initialValue: user.firstName,
                autocorrect: false,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "First Name",
                ),
                onChanged: (value) => {user.firstName = value},
                validator: (value) {
                  if (value != null && value != "") return null;
                  return "This field is necessary";
                },
                inputFormatters: [
                  new LengthLimitingTextInputFormatter(30),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                initialValue: user.lastName,
                autocorrect: false,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Last Name",
                ),
                onChanged: (value) => {user.lastName = value},
                validator: (value) {
                  if (value != null && value != "") return null;
                  return "This field is necessary";
                },
                inputFormatters: [
                  new LengthLimitingTextInputFormatter(30),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                initialValue: user.bio,
                autocorrect: true,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Short bio",
                ),
                onChanged: (value) => {user.bio = value},
                inputFormatters: [
                  new LengthLimitingTextInputFormatter(100),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                initialValue: user.phone,
                autocorrect: false,
                keyboardType: TextInputType.phone,
                decoration: InputDecorations.authInputDecoration(
                  labelText: "Phone Number",
                ),
                onChanged: (value) => {user.phone = value},
                validator: (value) {
                  if (value == null) {
                    return "This field is necessary";
                  } else if (double.tryParse(value) == null) {
                    return "Phone must be a number";
                  } else if (value.length != 10) {
                    return "Phone must be 10 digits long";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 34, 128, 99),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Center(
                  child: DropdownButton<String>(
                    value: selectedValue == "" ? "Other" : selectedValue,
                    items: <String>["Male", "Female", "Other"]
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      editForm.updateGender(value!);
                      selectedValue = value;
                    },
                    dropdownColor: Colors.orange[600],
                    style: const TextStyle(color: Colors.white, fontSize: 20),
                    icon: const Icon(
                      Icons.arrow_drop_down_circle_outlined,
                      size: 30,
                      color: AppTheme.secondary,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      const Color.fromARGB(255, 34, 128, 99),
                    ),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)))),
                onPressed: () async {
                  DateTime? selectedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime(2000),
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2022),
                      helpText: "SELECT YOUR BIRTH DAY",
                      builder: (context, child) => Theme(
                          data: ThemeData().copyWith(
                              colorScheme: const ColorScheme.light(
                            primary: AppTheme.primary,
                          )),
                          child: child!));
                  if (selectedDate != null) {
                    editForm.updateBirthDate(selectedDate);
                  }
                },
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Text(
                    "Birth date - ${user.birthDate.year}/${user.birthDate.month}/${user.birthDate.day}",
                    style: const TextStyle(color: Colors.white, fontSize: 17),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ));
  }
}
