import 'package:flutter/material.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/views.dart';
import 'package:provider/provider.dart';

class CheckAuthView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final userService = Provider.of<UserService>(context, listen: false);
    return Scaffold(
      body: Center(
          child: FutureBuilder(
        future: authService.readSavedEmail(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator(
              color: AppTheme.primary,
            );
          }
          if (snapshot.data == "") {
            Future.microtask(() {
              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder(
                      pageBuilder: (_, __, ___) => const LoginView(),
                      transitionDuration: const Duration(seconds: 0)));
            });
          } else {
            Future.microtask(() async {
              try {
                await userService.getUser(snapshot.data!.split(".")[0]);
                Navigator.pushReplacement(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => ProfileView(),
                        transitionDuration: const Duration(seconds: 0)));
              } catch (e) {
                Future.microtask(() {
                  Navigator.pushReplacement(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) => const LoginView(),
                          transitionDuration: const Duration(seconds: 0)));
                });
              }
            });
          }
          return Container();
        },
      )),
    );
  }
}
