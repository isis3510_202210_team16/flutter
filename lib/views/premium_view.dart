import 'package:flutter/material.dart';
import 'package:homeet/themes/app_theme.dart';

class PremiumView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final List<String> benefitsGold = <String>[
      "• Benefit1",
      "• Benefit2",
      "• Benefit3",
    ];

    final List<String> benefitsSilver = <String>[
      "• Benefit1",
      "• Benefit2",
      "• Benefit3",
    ];

    Widget List1() {
      return ListView.builder(
          padding: const EdgeInsets.only(top: 20),
          itemCount: benefitsGold.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 50,
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text(" ${benefitsGold[index]}")),
            );
          });
    }

    Widget card(List<String> beneficios, String name) {
      return Container(
        width: 600,
        height: 400,
        padding: new EdgeInsets.all(10.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: Colors.white,
          elevation: 10,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                name,
                style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: "Lato",
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 170,
              width: 1000,
              margin: EdgeInsets.only(left: 40, right: 40),
              child: List1(),
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(top: 60.0, bottom: 20),
                height: 45.0,
                width: 200.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                    color: AppTheme.primary),
                child: Center(
                  child: Text(
                    "Purchase",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: "Lato",
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ]),
        ),
      );
    }

    ;

    return SingleChildScrollView(
      child: Column(
        children: [
          card(benefitsGold, "Homeet Gold"),
          card(benefitsSilver, "Homeet silver")
        ],
      ),
    );
    ;
  }
}
  // TODO: implement build


