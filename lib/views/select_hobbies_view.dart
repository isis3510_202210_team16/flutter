import 'package:flutter/material.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:provider/provider.dart';

class SelectHobbiesView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final hobbieService = Provider.of<HobbieService>(context);
    final userService = Provider.of<UserService>(context);
    final _hobbies = hobbieService.hobbies;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (() => Navigator.pop(context)),
        backgroundColor: AppTheme.primary,
        child: const Icon(
          Icons.save,
          color: AppTheme.secondary,
        ),
      ),
      appBar: AppBar(title: const Text("Select your hobbies")),
      body: ListView.separated(
        itemCount: _hobbies.length,
        itemBuilder: (context, index) {
          return Container(
            color: (hobbieService.isHobbieSelected(
                    hobbieService.selectedHobbies, _hobbies[index]))
                ? Colors.green.withOpacity(0.5)
                : Colors.transparent,
            child: ListTile(
              onTap: () async {
                hobbieService.checkConnection();
                if (hobbieService.hasConnection) {
                  if (hobbieService.isHobbieSelected(
                      hobbieService.selectedHobbies, _hobbies[index])) {
                    userService.deleteHobbie(_hobbies[index]);
                    hobbieService.deselectHobbie(
                        _hobbies[index], userService.loggedUser.username!);
                  } else {
                    hobbieService.selectHobbie(
                        _hobbies[index], userService.loggedUser.username!);
                    userService.addHobbie(_hobbies[index]);
                  }
                } else {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: const Text("No internet"),
                      content: const Text(
                          "You seem to have lost connection, check for a network and try again"),
                      actions: [
                        TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: const Text(
                              "Ok",
                              style: TextStyle(color: AppTheme.primary),
                            ))
                      ],
                    ),
                  );
                }
              },
              title: Text(
                _hobbies[index].name,
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const Divider();
        },
      ),
    );
  }
}
