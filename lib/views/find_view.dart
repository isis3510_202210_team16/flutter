import 'package:flutter/material.dart';
import 'package:homeet/services/auth_service.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/listGroups_view.dart';
import 'package:homeet/views/login_view.dart';
import 'package:homeet/views/premium_view.dart';
import 'package:homeet/widgets/profile_info.dart';
import 'package:homeet/widgets/users_list_page.dart';
import 'package:homeet/widgets/users_list_page_v2.dart';
import 'package:provider/provider.dart';

class FindView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FindView();
  }
}

// ignore: non_constant_identifier_names
class _FindView extends State<FindView> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              bottom: TabBar(
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(50), // Creates border
                    color: Color.fromARGB(
                        255, 21, 129, 96)), //Change background color from here
                tabs: [
                  Tab(icon: Icon(Icons.person)),
                  Tab(icon: Icon(Icons.group))
                ],
              ),
            )),
        body: TabBarView(
          children: [
            UsersListPage2(),
            ListGroups(),
            //Icon(Icons.flight, size: 350),
          ],
        ),
      ),
    );
  }
}
