import 'package:flutter/material.dart';

class AppTheme {
  static const Color primary = Color.fromRGBO(33, 194, 147, 1); //21C293
  static const Color secondary = Color.fromRGBO(255, 232, 161, 1); //ffe8a1

  static final ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: primary,
    appBarTheme: const AppBarTheme(
      color: primary,
      elevation: 0,
      centerTitle: true,
    ),
    // inputDecorationTheme: const InputDecorationTheme(
    //   enabledBorder: OutlineInputBorder(
    //       borderSide: BorderSide(color: secondary),
    //       borderRadius: BorderRadius.all(Radius.circular(30))),
    //   focusedBorder: OutlineInputBorder(
    //       borderSide: BorderSide(color: secondary),
    //       borderRadius: BorderRadius.all(Radius.circular(30))),
    //   border: OutlineInputBorder(
    //       borderSide: BorderSide(color: secondary),
    //       borderRadius: BorderRadius.all(Radius.circular(30))),
    //   labelStyle: TextStyle(color: Colors.white),
    // )
  );
}
