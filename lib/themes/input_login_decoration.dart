import 'package:flutter/material.dart';
import 'package:homeet/themes/app_theme.dart';

class InputDecorations {
  static InputDecoration authInputDecoration({required String labelText}) {
    return InputDecoration(
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.secondary),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.secondary),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      border: const OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.secondary),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      labelStyle: const TextStyle(color: Colors.white),
      labelText: labelText,
    );
  }
}
