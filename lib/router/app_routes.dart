import 'package:flutter/material.dart';
import 'package:homeet/views/chatFeed_view.dart';
import 'package:homeet/views/groupDetail_view.dart';
import 'package:homeet/views/views.dart';

class AppRoutes {
  static const initialRoute = "checkAuth";

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};
    appRoutes.addAll({
      "login": (BuildContext context) => const LoginView(),
      "register": (BuildContext context) => const RegisterView(),
      "profile": (BuildContext context) => ProfileView(),
      "editProfile": (BuildContext context) => const EditProfileView(),
      "checkAuth": (BuildContext context) => CheckAuthView(),
      "selectHobbies": (BuildContext context) => SelectHobbiesView(),
      "chatFeed": (BuildContext context) => ChatFeedView(),
      "reserPassword": (BuildContext context) => const ResetPasswordScreen(),
    });
    return appRoutes;
  }
}
