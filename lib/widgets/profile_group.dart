import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homeet/models/hobbie.dart';
import 'package:homeet/services/services.dart';
import 'package:provider/provider.dart';

import '../models/models.dart';

class ProfileGroup extends StatelessWidget {
  final Group group;
  const ProfileGroup({Key? key, required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String>? events = group.events;

    final group_icon = Container(
      margin: EdgeInsets.only(top: 2.0, left: 25.0, right: 20.0),
      width: 80.0,
      height: 80.0,
      decoration: const BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/missing-hobbie-icon.png"))),
    );

    final groupTitle = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Text(
        group.name,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Lato", fontSize: 20.0, fontWeight: FontWeight.w900),
      ),
    );

    final group_members = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Text(
          "${group.members}",
          textAlign: TextAlign.left,
          style: TextStyle(
              fontFamily: "Lato",
              fontSize: 15.0,
              color: Color.fromARGB(255, 0, 0, 0)),
        ),
      ),
    );

    final hobbieDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        groupTitle,
        group_members,
      ],
    );

    return Row(
      children: <Widget>[group_icon, hobbieDetails],
    );
  }
}
