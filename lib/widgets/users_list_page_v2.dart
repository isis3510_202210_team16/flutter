import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:homeet/models/friend.dart';
import 'package:homeet/models/models.dart';
import 'package:homeet/services/user_service.dart';
import 'package:homeet/widgets/connectivity.dart';
import 'package:homeet/widgets/snackbars.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class UsersListPage2 extends StatefulWidget {
  @override
  _UsersListPageState2 createState() => _UsersListPageState2();
}

class _UsersListPageState2 extends State<UsersListPage2> {
  List<User>? _users = [];
  bool isLoading = false;

  @override
  void initState() {
    _loadFriends();
    super.initState();
  }

  Future<void> _loadFriends() async {
    setState(() {
      isLoading = true;
    });
    bool result = await connectionStatus.getNormalStatus();
    if (result) {
      try {
        final DatabaseReference usersRef =
            FirebaseDatabase.instance.ref("/users");
        final snapshot = await usersRef.get();
        _users = await UserService().getUsers2();
        setState(() {
          isLoading = false;
        });
      } on Exception catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      setState(() {
        isLoading = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      showDialog(
        context: context,
        builder: (_) => Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          elevation: 8,
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          child: Text('No tienes internet'),
        ),
      );
    }
  }

  Widget _buildFriendListTile(BuildContext context, int index) {
    var friend = _users![index];
    var list = [
      'HobbieTest1',
      'HobbieTest100',
      'HobbieTest105',
      'HobbieTest109',
      'HobbieTest113' 'HobbieTest12',
      'HobbieTest122',
      'HobbieTest171',
    ];
    var randomItem = (list..shuffle()).first;

    //var hob = friend.hobbies?.join(",");
    var hob = randomItem;

    return ListTile(
      //onTap: () => _navigateToFriendDetails(friend, index),
      leading: Hero(
        tag: index,
        child: CircleAvatar(
          //backgroundImage: new NetworkImage(friend.avatar),
          backgroundImage:
              CachedNetworkImageProvider(url_avatar(friend.avatar)),
        ),
      ),
      //title: new Text(friend.firstName + " " + friend.lastName),
      title: Text(friend.username!),

      //subtitle: new Text(friend.bio! + "\n" + hob),
      subtitle: Text(friend.bio!),
    );
  }

  // void _navigateToFriendDetails(Friend friend, Object avatarTag) {
  //   Navigator.of(context).push(
  //     new MaterialPageRoute(
  //       builder: (c) {
  //         return new FriendDetailsPage(friend, avatarTag: avatarTag);
  //       },
  //     ),
  //   );
  // }

  String url_avatar(String avatar) {
    if (avatar == "") {
      return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Missing_avatar.svg/638px-Missing_avatar.svg.png";
    } else {
      if (avatar.startsWith("http")) {
        return avatar;
      } else {
        return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Missing_avatar.svg/638px-Missing_avatar.svg.png";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget content;

    //final userService = Provider.of<UserService>(context);
    //_loadFriends();

    if (isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    } else if (_users!.isEmpty) {
      content = const Center(child: Text("No hay usuarios para mostrar"));
    } else {
      content = ListView.builder(
        itemCount: _users!.length,
        itemBuilder: _buildFriendListTile,
      );
    }

    return Scaffold(
      body: content,
    );
  }
}
