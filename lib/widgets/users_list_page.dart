import 'dart:async';

import 'package:google_fonts/google_fonts.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'package:flutter/material.dart';
import 'package:homeet/models/friend.dart';
import 'package:http/http.dart' as http;

class UsersListPage extends StatefulWidget {
  @override
  _UsersListPageState createState() => new _UsersListPageState();
}

class _UsersListPageState extends State<UsersListPage> {
  List<Friend> _friends = [];
  bool _hasInternet = false;

  @override
  void initState() {
    super.initState();
    _loadFriends();
  }

  Future<void> _loadFriends() async {
    bool checkConnetion = await InternetConnectionChecker().hasConnection;

    setState(() {
      _hasInternet = checkConnetion;
    });

    if (!_hasInternet) return;
    http.Response response =
        await http.get(Uri.parse('https://randomuser.me/api/?results=25'));

    setState(() {
      _friends = Friend.allFromResponse(response.body);
    });
  }

  Widget _buildFriendListTile(BuildContext context, int index) {
    var friend = _friends[index];

    return new ListTile(
      //onTap: () => _navigateToFriendDetails(friend, index),
      leading: new Hero(
        tag: index,
        child: new CircleAvatar(
          backgroundImage: new NetworkImage(friend.avatar),
        ),
      ),
      title: new Text(friend.name),
      subtitle: new Text(friend.hobby),
    );
  }

  // void _navigateToFriendDetails(Friend friend, Object avatarTag) {
  //   Navigator.of(context).push(
  //     new MaterialPageRoute(
  //       builder: (c) {
  //         return new FriendDetailsPage(friend, avatarTag: avatarTag);
  //       },
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    Widget content;

    if (!_hasInternet) {
      content = new Center(
          child: Text(
        "There is no internet connection",
        style: GoogleFonts.inter(fontSize: 30, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ));
    } else if (_friends.isEmpty) {
      content = new Center(
        child: new CircularProgressIndicator(),
      );
    } else {
      content = new ListView.builder(
        itemCount: _friends.length,
        itemBuilder: _buildFriendListTile,
      );
    }

    return new Scaffold(
      body: content,
    );
  }
}
