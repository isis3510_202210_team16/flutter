import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homeet/models/hobbie.dart';
import 'package:homeet/services/services.dart';
import 'package:provider/provider.dart';

class ProfileHobbie extends StatelessWidget {
  final Hobbie hobbie;
  const ProfileHobbie({Key? key, required this.hobbie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final hobbie_icon = Container(
      margin: EdgeInsets.only(top: 2.0, left: 25.0, right: 20.0),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          image: DecorationImage(fit: BoxFit.cover, image: getImage())),
    );

    final hobbieTitle = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Text(
        hobbie.name,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Lato", fontSize: 20.0, fontWeight: FontWeight.w900),
      ),
    );

    final hobbie_description = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Text(
          hobbie.category,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontFamily: "Lato",
              fontSize: 15.0,
              color: Color.fromARGB(255, 0, 0, 0)),
        ),
      ),
    );

    final hobbieDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        hobbieTitle,
        hobbie_description,
      ],
    );

    return Row(
      children: <Widget>[hobbie_icon, hobbieDetails],
    );
  }

  ImageProvider getImage() {
    if (hobbie.thumbnail == "") {
      return const AssetImage("assets/images/missing-hobbie-icon.png");
    } else if (hobbie.thumbnail.startsWith("http")) {
      return CachedNetworkImageProvider(
        hobbie.thumbnail,
      );
    }
    return FileImage(File(hobbie.thumbnail));
  }
}
