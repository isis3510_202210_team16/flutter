import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomSnackBars {
  static showSuccessSnackBar(message) => Get.showSnackbar(
        GetSnackBar(
          title: '¡Listo!',
          message: message,
          duration: const Duration(seconds: 5),
          backgroundColor: Colors.green,
        ),
      );
  static showErrorSnackBar(message) => Get.showSnackbar(
        GetSnackBar(
          title: 'Error',
          message: message,
          duration: const Duration(seconds: 5),
        ),
      );
}
