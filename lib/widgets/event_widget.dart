import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homeet/models/event.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:intl/intl.dart';

class EventCard extends StatelessWidget {
  final Event event;

  const EventCard({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(event.date);
    Widget List1() {
      return Container(
          padding: EdgeInsets.only(top: 30),
          child: Column(children: [
            Container(
              padding: EdgeInsets.only(left: 40),
              child: Row(
                children: [
                  Icon(Icons.location_on, color: AppTheme.primary),
                  Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(event.location))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 40, top: 20),
              child: Row(
                children: [
                  Icon(Icons.calendar_month, color: AppTheme.primary),
                  Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(formattedDate))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 40, top: 20),
              child: Row(
                children: [
                  Icon(Icons.local_atm, color: AppTheme.primary),
                  Container(
                      padding: EdgeInsets.only(left: 20),
                      child: (event.price == 0)
                          ? Text("Free")
                          : Text('${event.price}'))
                ],
              ),
            )
          ]));
    }

    Widget card() {
      return Container(
        width: 600,
        height: 320,
        padding: new EdgeInsets.all(10.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: Colors.white,
          elevation: 10,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                event.name,
                style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: "Lato",
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 170,
              width: 700,
              margin: EdgeInsets.only(left: 40, right: 40),
              child: List1(),
            ),
            InkWell(
              child: Container(
                height: 45.0,
                width: 200.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                    color: AppTheme.primary),
                child: Center(
                  child: Text(
                    "Confirm assitance",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: "Lato",
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ]),
        ),
      );
    }

    // TODO: implement build
    return card();
  }
}
