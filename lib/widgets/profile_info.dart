import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homeet/models/group.dart';
import 'package:homeet/models/hobbie.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:homeet/views/groupDetail_view.dart';
import 'package:homeet/widgets/profile_description.dart';
import 'package:homeet/widgets/profile_group.dart';
import 'package:homeet/widgets/profile_hobbie.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';

class ProfileInfo extends StatelessWidget {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context);
    final hobbieService = Provider.of<HobbieService>(context);
    final groupService = Provider.of<GroupService>(context);
    hobbieService.checkConnection();
    groupService.checkConnection();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          Stack(
            children: [
              Container(
                  height: 155.0,
                  decoration: const BoxDecoration(color: AppTheme.primary)),
              Column(
                children: [
                  Container(
                      margin: const EdgeInsets.only(
                        top: 20.0,
                      ),
                      child: CircleAvatar(
                        backgroundImage: getImage(userService),
                        radius: 85,
                      )),
                  ProfileDescription(),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 2),
                          child: InkWell(
                            onTap: () => userService.changeHobbieGroup(true),
                            child: Container(
                              height: 50.0,
                              width: 130.0,
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(30),
                                      bottomLeft: Radius.circular(30)),
                                  color: userService.showHobbies
                                      ? AppTheme.primary
                                      : AppTheme.primary.withOpacity(0.5)),
                              child: Center(
                                child: Text(
                                  "Hobbies",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontFamily: "Lato",
                                      color: userService.showHobbies
                                          ? Colors.white
                                          : Colors.grey[600]),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 2),
                          child: InkWell(
                            onTap: () => userService.changeHobbieGroup(false),
                            child: Container(
                              height: 50.0,
                              width: 130.0,
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      bottomRight: Radius.circular(30)),
                                  color: !userService.showHobbies
                                      ? AppTheme.primary
                                      : AppTheme.primary.withOpacity(0.5)),
                              child: Center(
                                child: Text(
                                  "Grupos",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontFamily: "Lato",
                                      color: !userService.showHobbies
                                          ? Colors.white
                                          : Colors.grey[600]),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          (userService.showHobbies)
              ? Column(
                  children: [
                    getHobbiesWidgets(
                        userService.loggedUser.hobbies, hobbieService),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 100, vertical: 10),
                      child: MaterialButton(
                          onPressed: hobbieService.isLoading
                              ? null
                              : () async {
                                  bool hasInternet =
                                      await InternetConnectionChecker()
                                          .hasConnection;
                                  if (hasInternet) {
                                    hobbieService.setLoading(true);
                                    hobbieService.getAllHobbies().then((value) {
                                      hobbieService
                                          .setSelectedHobbies(
                                              userService.loggedUser.hobbies)
                                          .then((value) {
                                        Navigator.of(context)
                                            .pushNamed("selectHobbies");
                                        hobbieService.setLoading(false);
                                      });
                                    });
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: const Text("No internet"),
                                        content: const Text(
                                            "There is no internet connection, check for a network and try again"),
                                        actions: [
                                          TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              child: const Text(
                                                "Ok",
                                                style: TextStyle(
                                                    color: AppTheme.primary),
                                              ))
                                        ],
                                      ),
                                    );
                                  }
                                },
                          disabledColor: Colors.grey,
                          elevation: 0,
                          color: AppTheme.primary,
                          child: Container(
                              width: double.infinity,
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              child: Text(
                                hobbieService.isLoading ? "Loading ..." : "+",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 18),
                                textAlign: TextAlign.center,
                              )),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30))),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                  ],
                )
              : Column(
                  children: [
                    getGroupsWidgets(
                        userService.loggedUser.groups, groupService),
                  ],
                )
        ]),
      ),
    );
  }

  ImageProvider getImage(UserService userService) {
    if (userService.loggedUser.avatar == "") {
      return const AssetImage("assets/images/missing-avatar.png");
    } else if (userService.loggedUser.avatar.startsWith("http")) {
      return CachedNetworkImageProvider(
        userService.loggedUser.avatar,
      );
    }
    return FileImage(File(userService.loggedUser.avatar));
  }

  Widget getHobbiesWidgets(List<String>? strings, HobbieService hobbieService) {
    if (!hobbieService.hasConnection) {
      return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Text(
          "There is no internet connection to load your hobbies",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else if (strings == null || strings.isEmpty) {
      return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Text(
          "You haven't select any hobbie, add some below!!",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return Column(
          children: strings
              .map(
                (item) => FutureBuilder(
                  future: hobbieService.getHobbieByName(item),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CircularProgressIndicator(
                          color: AppTheme.primary,
                        ),
                      );
                    }
                    if (snapshot.data == null) {
                      return const Text(
                          "There was an error loading the info hobbie");
                    } else {
                      Hobbie hob = snapshot.data as Hobbie;
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: ProfileHobbie(hobbie: hob),
                          ),
                          const Divider()
                        ],
                      );
                    }
                  },
                ),
              )
              .toList());
    }
  }

  Widget getGroupsWidgets(List<String>? strings, GroupService groupService) {
    if (!groupService.hasConnection) {
      return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Text(
          "There is no internet connection to load your groups",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else if (strings == null || strings.isEmpty) {
      return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Text(
          "You haven't select any group",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return Column(
          children: strings
              .map(
                (item) => FutureBuilder(
                  future: groupService.getGroup(item),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CircularProgressIndicator(
                          color: AppTheme.primary,
                        ),
                      );
                    }
                    if (snapshot.data == null) {
                      return const Text(
                          "There was an error loading the info group");
                    } else {
                      Group grupo = snapshot.data as Group;
                      return Column(
                        children: [
                          Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: GestureDetector(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          GroupDetail(group: grupo)),
                                ),
                                child: ProfileGroup(group: grupo),
                              )),
                          const Divider()
                        ],
                      );
                    }
                  },
                ),
              )
              .toList());
    }
  }
}
