import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectionStatus {
  Future<bool> getNormalStatus() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      print('I am connected to a mobile network ---------------');
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      print('I am connected to a wifi network ---------------');
      return true;
    } else {
      //Get.toNamed(Routes.NO_INTERNET);
      print('I am not connected to network');
      return false;
    }
  }
}

ConnectionStatus connectionStatus = ConnectionStatus();
