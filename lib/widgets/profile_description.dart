import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homeet/services/services.dart';
import 'package:provider/provider.dart';

class ProfileDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context);
    final profile_name = Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: 20.0,
          ),
          child: Text(
            "${userService.loggedUser.firstName} ${userService.loggedUser.lastName}",
            style: GoogleFonts.inter(fontSize: 30, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          alignment: Alignment.bottomCenter,
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          child: Text(
            " '${userService.loggedUser.bio}'",
            style: GoogleFonts.inter(),
            textAlign: TextAlign.center,
          ),
          alignment: Alignment.bottomCenter,
        )
      ],
    );

    return profile_name;
  }
}
