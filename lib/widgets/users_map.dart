import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geolocator_android/geolocator_android.dart';
import 'package:geolocator_apple/geolocator_apple.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:homeet/models/models.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/widgets/connectivity.dart';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  @override
  static const _initialCameraPosition = CameraPosition(
    target: LatLng(4.60971, -74.08175),
    zoom: 11.5,
  );

  late GoogleMapController _googleMapController;
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }

  List<Marker> allMarkers = [];
  bool isLoading = false;
  @override
  void initState() {
    getData();
    super.initState();
  }

  List<User> _users = [];
  getData() async {
    setState(() {
      isLoading = true;
    });

    bool result = await connectionStatus.getNormalStatus();

    if (result) {
      try {
        _users = (await UserService().getUsers2())!;
        _addMarker(_users);
        setState(() {
          isLoading = false;
        });
      } on Exception catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      setState(() {
        isLoading = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      showDialog(
        context: context,
        builder: (_) => Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          elevation: 8,
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          child: Text('No tienes internet'),
        ),
      );
    }
  }

  String url_avatar(String avatar) {
    if (avatar == "") {
      return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Missing_avatar.svg/638px-Missing_avatar.svg.png";
    } else {
      if (avatar.startsWith("http")) {
        return avatar;
      } else {
        return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Missing_avatar.svg/638px-Missing_avatar.svg.png";
      }
    }
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  late Uint8List originalUnit8List;
  late Uint8List targetlUinit8List;

  Future<Uint8List> resizeImage(String imageUrl) async {
    //String imageUrl = 'https://picsum.photos/250?image=9';
    http.Response response = await http.get(Uri.parse(imageUrl));
    Uint8List originalUnit8List = response.bodyBytes;

    ui.Image originalUiImage = await decodeImageFromList(originalUnit8List);
    ByteData? originalByteData = await originalUiImage.toByteData();
    print('original image ByteData size is ${originalByteData?.lengthInBytes}');

    var codec = await ui.instantiateImageCodec(originalUnit8List,
        targetHeight: 12, targetWidth: 12);
    var frameInfo = await codec.getNextFrame();
    ui.Image targetUiImage = frameInfo.image;

    ByteData? targetByteData =
        await targetUiImage.toByteData(format: ui.ImageByteFormat.png);
    print('target image ByteData size is ${targetByteData!.lengthInBytes}');
    Uint8List targetlUinit8List = targetByteData.buffer.asUint8List();
    return targetlUinit8List;

    setState(() {});
  }

  Future<void> _addMarker(List<User> usuarios) async {
    usuarios.forEach((element) async {
      //String imgurl = "https://www.fluttercampus.com/img/car.png";
      String imgurl = url_avatar(element.avatar);

      Uint8List bytes =
          (await NetworkAssetBundle(Uri.parse(imgurl)).load(imgurl))
              .buffer
              .asUint8List();

      //resizeImage(imgurl);
      print(element.username);
      Marker _marker = Marker(
        markerId: MarkerId(element.username!),
        infoWindow: InfoWindow(title: element.username, snippet: element.bio),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        //position: LatLng(4.60971, -74.08175),

        position: LatLng(element.lat!, element.long!),
        //icon: BitmapDescriptor.fromBytes(targetlUinit8List),
      );
      allMarkers.add(_marker);
    });
    setState(() {
      //offer.latitude = pos.latitude;
      //offer.longitude = pos.longitude;
    });
  }

  Future<Position> _determinePosition() async {
    _registerPlatformInstance();
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      return Future.error('Location services are disabled');
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        return Future.error("Location permission denied");
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition();

    return position;
  }

  void _registerPlatformInstance() {
    if (Platform.isAndroid) {
      GeolocatorAndroid.registerWith();
    } else if (Platform.isIOS) {
      GeolocatorApple.registerWith();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        //mapType: MapType.hybrid,
        myLocationButtonEnabled: false,
        zoomControlsEnabled: false,
        zoomGesturesEnabled: true,
        initialCameraPosition: _initialCameraPosition,
        markers: Set.from(allMarkers),
        onMapCreated: (controller) => _googleMapController = controller,
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          foregroundColor: Colors.black,
          child: const Icon(Icons.my_location),
          onPressed: () async {
            Position position = await _determinePosition();

            _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(
                    target: LatLng(position.latitude, position.longitude),
                    zoom: 14)));

            allMarkers.add(Marker(
                markerId: const MarkerId('currentLocation'),
                position: LatLng(position.latitude, position.longitude)));

            setState(() {});
          }),
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
