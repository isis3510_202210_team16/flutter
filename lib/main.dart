import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:homeet/router/app_routes.dart';
import 'package:homeet/services/hobbie_service.dart';
import 'package:homeet/services/services.dart';
import 'package:homeet/themes/app_theme.dart';
import 'package:provider/provider.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'services/event_service.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(AppState());
}

class AppState extends StatelessWidget {
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fbApp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print(
              "There was an error initializing firebase app ${snapshot.error.toString()}");
          return Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.red,
            child: const Text(
              "There was an error loading the app",
              style: TextStyle(color: Colors.black),
            ),
          );
        } else if (snapshot.hasData) {
          FlutterError.onError =
              FirebaseCrashlytics.instance.recordFlutterError;
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => AuthService()),
              ChangeNotifierProvider(create: (_) => UserService()),
              ChangeNotifierProvider(create: (_) => HobbieService()),
              ChangeNotifierProvider(create: (_) => GroupService()),
              ChangeNotifierProvider(create: (_) => EventService()),
            ],
            child: MyApp(),
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // FirebaseCrashlytics.instance.crash();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Homeet',
      initialRoute: AppRoutes.initialRoute,
      routes: AppRoutes.getAppRoutes(),
      theme: AppTheme.lightTheme,
      scaffoldMessengerKey: NotificationsService.messangerKey,
    );
  }
}
