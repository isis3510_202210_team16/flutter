import 'dart:convert';

import 'package:homeet/models/group.dart';

class Event {
  String name;
  DateTime date;
  String location;
  double price;
  //Group grupo;

  Event({
    required this.name,
    required this.date,
    required this.location,
    required this.price,
    //required this.grupo,
  });

  factory Event.fromJson(String str) => Event.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Event.fromMap(Map<String, dynamic> jsonMap) => Event(
        name: jsonMap["name"],
        date: jsonMap["date"],
        location: jsonMap["location"],
        price: jsonMap["price"],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "location": location,
        "price": price,
      };

  Event copy() =>
      Event(name: name, date: date, location: location, price: price);
}
