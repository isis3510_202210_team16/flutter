import 'dart:convert';

class Group {
  String name;
  int members;
  bool private;
  String description;
  String image;
  List<String>? users;
  List<String>? events;

  Group({
    required this.name,
    required this.members,
    required this.private,
    required this.description,
    required this.image,
    this.users,
    this.events,
  });

  factory Group.fromJson(String str) => Group.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Group.fromMap(Map<String, dynamic> jsonMap) => Group(
      name: jsonMap["name"],
      members: jsonMap["members"],
      private: jsonMap["private"] ?? false,
      description: jsonMap["description"],
      image: jsonMap["image"],
      users: jsonMap["users"] == null || jsonMap["users"] == false
          ? null
          : Group.toUserList(jsonMap["users"] as Map<String, dynamic>),
      events: jsonMap["events"] == null || jsonMap["events"] == false
          ? null
          : Group.toEventList(jsonMap["events"] as Map<String, dynamic>));

  static List<String> toUserList(Map<String, dynamic> jsonMap) {
    List<String> users = List.empty(growable: true);

    jsonMap.forEach((key, value) {
      users.add(key);
    });
    return users;
  }

  static List<String> toEventList(Map<String, dynamic> jsonMap) {
    List<String> events = List.empty(growable: true);

    jsonMap.forEach((key, value) {
      events.add(key);
    });
    return events;
  }

  Map<String, dynamic> toMap() => {
        "name": name,
        "members": members,
        "private": private,
        "description": description,
        "image": image,
        "users": users == null ? "" : Group.toUserMap(users!).toString(),
        "events": events == null ? "" : Group.toEventMap(events!).toString(),
      };

  static Map<String, bool> toUserMap(List<String> users) {
    Map<String, bool> map = {for (var user in users) user: true};
    return map;
  }

  static Map<String, bool> toEventMap(List<String> events) {
    Map<String, bool> map = {for (var event in events) event: true};
    return map;
  }

  Group copy() => Group(
      name: name,
      members: members,
      private: private,
      description: description,
      image: image);
}
