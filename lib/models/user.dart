import 'dart:convert';

class User {
  User(
      {required this.birthDate,
      required this.firstName,
      required this.gender,
      required this.lastName,
      required this.phone,
      required this.hasData,
      required this.avatar,
      this.username,
      this.bio,
      this.lat,
      this.long,
      this.hobbies,
      this.groups});

  DateTime birthDate;
  String firstName;
  String gender;
  String lastName;
  String phone;
  bool hasData;
  String avatar;
  List<String>? hobbies;
  List<String>? groups;
  String? bio;
  double? lat;
  double? long;
  String? username;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> jsonMap) => User(
        birthDate: DateTime.parse(jsonMap["birthDate"]),
        firstName: jsonMap["firstName"],
        gender: jsonMap["gender"],
        lastName: jsonMap["lastName"],
        phone: jsonMap["phone"],
        hasData: jsonMap["hasData"],
        bio: jsonMap["bio"],
        lat: jsonMap["lat"],
        long: jsonMap["long"],
        avatar: jsonMap["avatar"],
        hobbies: jsonMap["hobbies"] == null || jsonMap["hobbies"] == ""
            ? null
            : User.toHobbieList(jsonMap["hobbies"]),
        groups: jsonMap["groups"] == null || jsonMap["groups"] == ""
            ? null
            : User.toGroupList(jsonMap["groups"]),
      );

  static List<String> toHobbieList(Map jsonMap) {
    List<String> hobbies = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      hobbies.add(key);
    });
    return hobbies;
  }

  static List<String> toGroupList(Map jsonMap) {
    List<String> groups = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      groups.add(key);
    });
    return groups;
  }

  Map<String, dynamic> toMap() => {
        "birthDate":
            "${birthDate.year.toString().padLeft(4, '0')}-${birthDate.month.toString().padLeft(2, '0')}-${birthDate.day.toString().padLeft(2, '0')}",
        "firstName": firstName,
        "gender": gender,
        "lastName": lastName,
        "phone": phone,
        "hasData": hasData,
        "bio": bio,
        "lat": lat,
        "long": long,
        "avatar": avatar,
        "hobbies": hobbies == null ? "" : User.toHobbieMap(hobbies!),
        "groups": groups == null ? "" : User.toGroupMap(groups!)
      };

  User copy() => User(
      birthDate: birthDate,
      firstName: firstName,
      gender: gender,
      lastName: lastName,
      phone: phone,
      hasData: hasData,
      username: username,
      bio: bio,
      avatar: avatar,
      hobbies: hobbies,
      groups: groups);

  static Map<String, bool> toHobbieMap(List<String> hobbies) {
    Map<String, bool> map = {for (var hobbie in hobbies) hobbie: true};
    return map;
  }

  static Map<String, bool> toGroupMap(List<String> groups) {
    Map<String, bool> map = {for (var group in groups) group: true};
    return map;
  }

  static List<User> allFromResponse(String response) {
    var decodedJson = json.decode(response).cast<String, dynamic>();

    return decodedJson['results']
        .cast<Map<String, dynamic>>()
        .map((obj) => User.fromMapUser(obj))
        .toList()
        .cast<User>();
  }

  static User fromMapUser(Map map) {
    return User(
        birthDate: DateTime.parse(map['birthDate']),
        firstName: map['firstName'],
        gender: map['gender'],
        lastName: map['lastName'],
        phone: map['phone'],
        hasData: true,
        bio: map['bio'],
        lat: map['lat'],
        long: map['long'],
        avatar: map['avatar'],
        hobbies: (jsonDecode(map['hobbies']) as List<dynamic>).cast<String>());
  }
}
