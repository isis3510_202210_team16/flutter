import 'dart:convert';

class Hobbie {
  Hobbie({
    required this.name,
    required this.category,
    required this.thumbnail,
    this.users,
  });

  String name;
  String category;
  String thumbnail;
  List<String>? users;

  factory Hobbie.fromJson(String str) => Hobbie.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Hobbie.fromMap(Map<String, dynamic> jsonMap) => Hobbie(
        name: jsonMap["name"],
        category: jsonMap["category"],
        thumbnail: jsonMap["thumbnail"],
        users: Hobbie.toUserList(json.decode(jsonMap["users"])),
      );

  static List<String> toUserList(Map<String, dynamic> jsonMap) {
    List<String> users = List.empty();
    jsonMap.forEach((key, value) {
      users.add(key);
    });
    return users;
  }

  Map<String, dynamic> toMap() => {
        "name": name,
        "category": category,
        "thumbnail": thumbnail,
        "users": users == null ? "" : Hobbie.toUserMap(users!).toString(),
      };

  static Map<String, bool> toUserMap(List<String> users) {
    Map<String, bool> map = {for (var user in users) user: true};
    return map;
  }
}
