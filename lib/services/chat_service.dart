import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:homeet/models/models.dart';

class ChatService extends ChangeNotifier {
  ChatService();

  createChatRoom(String idChatRoom, chatRoomMap) {
    final DatabaseReference chatRef = FirebaseDatabase.instance.ref("/chats");
    chatRef.set(chatRoomMap).catchError((e) {
      print(e.toString());
    });
  }
}
