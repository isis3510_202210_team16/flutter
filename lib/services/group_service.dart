import 'dart:convert';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:homeet/models/event.dart';
import 'package:homeet/models/models.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class GroupService extends ChangeNotifier {
  List<Group> joinedGroups = List.empty(growable: true);

  final String _baseUrl = "homeet-827c1-default-rtdb.firebaseio.com";
  bool isLoading = true;

  late Group grupo;

  File? newAvatarFile;

  GroupService();

  List<Group> groups = List.empty();

  bool hasConnection = true;

  Future<Group?> getGroup(String groupName) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return null;
    isLoading = true;

    Group? group;

    const storage = FlutterSecureStorage();

    final url = Uri.https(_baseUrl, "groups/$groupName.json",
        {'auth': await storage.read(key: "token") ?? ""});

    final resp = await http.get(url);

    final Map<String, dynamic> groupMap = json.decode(resp.body);

    group = Group.fromMap(groupMap);

    grupo = group.copy();

    isLoading = false;

    return group;
  }

  Future<List<Group>?> getAllGroups() async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return null;
    final DatabaseReference hobbieRef =
        FirebaseDatabase.instance.ref("/groups");
    final snapshot = await hobbieRef.get();
    if (snapshot.exists) {
      Map data = (snapshot.value as Map);
      groups = toGroupList(data);
    } else {
      print("No groups");
      notifyListeners();
      return null;
    }
    return groups;
  }

  List<Group> toGroupList(Map jsonMap) {
    List<Group> groups = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      Map groupInfo = value as Map;
      List<String> values = toValueList(groupInfo);
      Group g = Group(
          image: values[0],
          name: values[3],
          description: values[4],
          members: int.parse(values[2]),
          private: values[1].toLowerCase() == 'true',
          users: values[6] == "false" ? null : toUserList(values[6]),
          events: values[5] == "false" ? null : toEventList(values[5]));

      groups.add(g);
    });
    return groups;
  }

  Future<void> joinGroup(Group group, String username) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return;
    joinedGroups.add(group);
    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("groups/${group.name}/users");
    dbRef.update({username: true});
    notifyListeners();
  }

  List<String> toValueList(Map jsonMap) {
    List<String> values = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      values.add("$value");
    });
    return values;
  }

  void addEvent(String event, Group g) {
    print("/////////////////");
    print(event);
    print("/////////////////");
    g.events == null
        ? g.events = List<String>.from([event], growable: true)
        : g.events!.add(event);

    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("groups/${g.name}/events");
    dbRef.update({event: true});

    notifyListeners();
  }

  List<String> toUserList(String userMap) {
    String strList = userMap
        .replaceAll("{", "")
        .replaceAll("}", "")
        .replaceAll(": true", "")
        .replaceAll(" ", "");
    //print(strList);
    List<String> events = strList.split(",");
    return events;
  }

  List<String> toEventList(String eventMap) {
    String strList = eventMap
        .replaceAll("{", "")
        .replaceAll("}", "")
        .replaceAll(": true", "")
        .replaceAll(" ", "");
    //print(strList);
    List<String> events = strList.split(",");
    return events;
  }

  void setLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  void checkConnection() async {
    hasConnection = await InternetConnectionChecker().hasConnection;
  }
}
