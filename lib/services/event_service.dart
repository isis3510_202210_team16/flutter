import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:homeet/models/event.dart';
import 'package:homeet/models/models.dart';
import 'package:http/http.dart' as http;

class EventService extends ChangeNotifier {
  final String _baseUrl = "homeet-827c1-default-rtdb.firebaseio.com";

  final storage = const FlutterSecureStorage();

  late Event levent;
  EventService();
  bool isLoading = true;

  Future<Event?> getEventByName(String event) async {
    final DatabaseReference hobbieRef =
        FirebaseDatabase.instance.ref("/events/$event");
    final snapshot = await hobbieRef.get();
    if (snapshot.exists) {
      Map eventInfo = snapshot.value as Map;
      List<String> values = toValueList(eventInfo);
      print(values);
      return Event(
          name: event,
          date: DateTime.parse(values[0]),
          location: values[3],
          price: double.parse(values[1]));
    } else {
      print("No event found with name $event");
      return null;
    }
  }

  Future<String?> createEvent(String name, DateTime dateTime, String location,
      double price, String eventId) async {
    print("llegó a create event");
    print(eventId);
    Event event = Event(
      date: dateTime,
      name: name,
      location: location,
      price: price,
    );
    print(event.toJson());
    print("se creó el event");

    isLoading = true;
    notifyListeners();

    final url = Uri.https(_baseUrl, "events/$eventId.json",
        {"auth": await storage.read(key: "token") ?? ""});

    try {
      await http.put(url, body: event.toJson());
      print("se puso en la base datos");
    } catch (e) {
      return "There was an error creating the event";
    }

    levent = event.copy();

    isLoading = false;
    notifyListeners();

    return null;
  }

  List<String> toValueList(Map jsonMap) {
    List<String> values = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      values.add("$value");
    });
    return values;
  }
}
