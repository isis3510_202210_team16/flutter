import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:homeet/models/models.dart';

import 'package:http/http.dart' as http;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_database/firebase_database.dart';

class UserService extends ChangeNotifier {
  final String _baseUrl = "homeet-827c1-default-rtdb.firebaseio.com";

  bool isLoading = true;
  bool isSaving = false;
  bool showHobbies = true;
  List<User> users = List.empty();
  List<String> users_strings = List.empty();

  final storage = const FlutterSecureStorage();

  late User loggedUser;

  File? newAvatarFile;

  UserService();

  Future<User?> getUser(String username) async {
    isLoading = true;
    notifyListeners();

    User? user;

    final url = Uri.https(_baseUrl, "users/$username.json",
        {'auth': await storage.read(key: "token") ?? ""});

    final resp = await http.get(url);
    final Map<String, dynamic> userMap = json.decode(resp.body);
    user = User.fromMap(userMap);
    user.username = username;
    loggedUser = user.copy();
    isLoading = false;
    notifyListeners();

    return user;
  }

  Future<String?> createUser(String username) async {
    User user = User(
        birthDate: DateTime.now(),
        firstName: "",
        gender: "Other",
        lastName: "",
        phone: "",
        bio: "",
        lat: 4.60971,
        long: -74.08175,
        avatar: "",
        hasData: false,
        username: username);

    isLoading = true;
    notifyListeners();

    final url = Uri.https(_baseUrl, "users/$username.json",
        {"auth": await storage.read(key: "token") ?? ""});

    try {
      await http.put(url, body: user.toJson());
    } catch (e) {
      return "There was an error creating the user";
    }

    loggedUser = user.copy();

    isLoading = false;
    notifyListeners();

    return null;
  }

  Future saveUserProfile(User user) async {
    isSaving = true;
    notifyListeners();

    final url = Uri.https(_baseUrl, "users/${user.username}.json",
        {"auth": await storage.read(key: "token") ?? ""});

    try {
      await http.put(url, body: user.toJson());
    } catch (e) {
      return "There was an error updating the user info";
    }

    isSaving = false;
    notifyListeners();
  }

  Future<void> uploadAvatarImage() async {
    final firebase_storage.FirebaseStorage storage =
        firebase_storage.FirebaseStorage.instance;

    if (newAvatarFile == null) {
      return;
    }

    File file = newAvatarFile!;
    String fileName = "avatar-${loggedUser.username}";

    final firebase_storage.Reference reference =
        storage.ref("user-avatars/$fileName");

    try {
      await reference.putFile(file);
      final DatabaseReference dbRef =
          FirebaseDatabase.instance.ref("users/${loggedUser.username!}");
      reference.getDownloadURL().then((value) {
        dbRef.update({"avatar": value});
        loggedUser.avatar = value;
      });
    } catch (e) {
      // print(e);
    }
  }

  Future<String> readToken() async {
    return await storage.read(key: "token") ?? "";
  }

  void updateAvatarImage(String path) {
    newAvatarFile = File(path);
    loggedUser.avatar = path;
    notifyListeners();
  }

  Future<void> addHobbie(Hobbie hobbie) async {
    loggedUser.hobbies == null
        ? loggedUser.hobbies = List<String>.from([hobbie.name], growable: true)
        : loggedUser.hobbies!.add(hobbie.name);
    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("users/${loggedUser.username}/hobbies");
    dbRef.update({hobbie.name: true});

    notifyListeners();
  }

  Future<void> deleteHobbie(Hobbie hobbie) async {
    if (loggedUser.hobbies == null) {
      return;
    }
    loggedUser.hobbies!.remove(hobbie.name);
    final DatabaseReference dbRef = FirebaseDatabase.instance
        .ref("users/${loggedUser.username}/hobbies/${hobbie.name}");
    dbRef.remove();
    notifyListeners();
  }

  Future<void> addGroup(Group group) async {
    loggedUser.groups == null
        ? loggedUser.groups = List<String>.from([group.name], growable: true)
        : loggedUser.groups!.add(group.name);
    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("users/${loggedUser.username}/groups");
    dbRef.update({group.name: true});

    notifyListeners();
  }

  Future<void> deleteGroup(Group group) async {
    if (loggedUser.groups == null) {
      return;
    }
    loggedUser.groups!.remove(group.name);
    final DatabaseReference dbRef = FirebaseDatabase.instance
        .ref("users/${loggedUser.username}/groups/${group.name}");
    dbRef.remove();
    notifyListeners();
  }

  void changeHobbieGroup(bool newView) {
    showHobbies = newView;
    notifyListeners();
  }

  Future<List<User>?> getUsers2() async {
    final DatabaseReference usersRef = FirebaseDatabase.instance.ref("/users");
    final snapshot = await usersRef.get();
    if (snapshot.exists) {
      Map data = (snapshot.value as Map);
      users = toUsersList(data);
    } else {
      notifyListeners();
      return null;
    }

    notifyListeners();
    return users;
  }

  Future<List<String>?> getUsersStrings() async {
    final DatabaseReference usersRef = FirebaseDatabase.instance.ref("/users");
    final snapshot = await usersRef.get();
    if (snapshot.exists) {
      Map data = (snapshot.value as Map);
      users_strings = toUsersListString(data);
    } else {
      notifyListeners();
      return null;
    }

    notifyListeners();
    return users_strings;
  }

  List<String> toUsersListString(Map jsonMap) {
    List<String> users_strings = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      users_strings.add(key);
    });
    return users_strings;
  }

  List<User> toUsersList(Map jsonMap) {
    List<User> users = List.empty(growable: true);
    List<String> users_strings = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      Map userInfo = value as Map;
      // Map ho = userInfo["hobbies"] as Map;
      //var list = getKeysFromMap(ho);
      //var stringList = list.join("");

      List<String> values = toValueList(userInfo);
      users_strings.add(key);
      users.add(User(
        username: key,
        //birthDate: DateTime.parse(jsonMap["birthDate"]),
        birthDate: DateTime.now(),
        firstName: userInfo["firstName"],
        gender: userInfo["gender"],
        lastName: userInfo["lastName"],
        phone: userInfo["phone"],
        hasData: userInfo["hasData"],
        bio: userInfo["bio"],
        lat: userInfo["lat"],
        long: userInfo["long"],
        avatar: userInfo["avatar"],
        //hobbies: list

        //username: key,
        //birthDate: DateTime.parse(jsonMap["birthDate"]),
        //birthDate: DateTime.now(),
        //firstName: values[0],
        //gender: values[4],
        //lastName: values[1],
        //phone: values[0],
        //hasData: jsonMap[values[3]],
        //hasData: true,
        //bio: values[5],
        //avatar: values[6],
        //avatar: userInfo["avatar"],
        //hobbies: (jsonDecode(values[3]) as List<dynamic>).cast<String>()
      ));
    });
    return users;
  }

  List<String> toValueList(Map jsonMap) {
    List<String> values = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      values.add("$value");
    });
    return values;
  }

  List<String> getKeysFromMap(Map map) {
    List<String> values = List.empty(growable: true);
    // Get all keys
    map.keys.forEach((key) {
      values.add("$key");
    });
    return values;
  }

  List<String> toHobbieList(Map jsonMap) {
    List<String> hobbies = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      Map hobbieInfo = value as Map;
      List<String> values = toValueList(hobbieInfo);
      hobbies.add(key);
    });
    return hobbies;
  }
}
