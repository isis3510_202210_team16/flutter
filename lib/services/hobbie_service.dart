import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:homeet/models/models.dart';

import 'package:internet_connection_checker/internet_connection_checker.dart';

class HobbieService extends ChangeNotifier {
  HobbieService();

  List<Hobbie> selectedHobbies = List.empty(growable: true);
  List<Hobbie> hobbies = List.empty();
  bool isLoading = false;
  bool hasConnection = true;

  Future<Hobbie?> getHobbieByName(String hobbie) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return null;
    final DatabaseReference hobbieRef =
        FirebaseDatabase.instance.ref("/hobbies/$hobbie");
    final snapshot = await hobbieRef.get();
    if (snapshot.exists) {
      Map hobbieInfo = snapshot.value as Map;
      List<String> values = toValueList(hobbieInfo);
      return Hobbie(name: hobbie, category: values[1], thumbnail: values[0]);
    } else {
      // print("No hobbie found with name $hobbies");
      return null;
    }
  }

  Future<List<Hobbie>?> getAllHobbies() async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return null;
    final DatabaseReference hobbieRef =
        FirebaseDatabase.instance.ref("/hobbies");
    final snapshot = await hobbieRef.get();
    if (snapshot.exists) {
      Map data = (snapshot.value as Map);
      hobbies = toHobbieList(data);
    } else {
      // print("No hobbies");
      notifyListeners();
      return null;
    }

    notifyListeners();
    return hobbies;
  }

  List<Hobbie> toHobbieList(Map jsonMap) {
    List<Hobbie> hobbies = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      Map hobbieInfo = value as Map;
      List<String> values = toValueList(hobbieInfo);
      hobbies.add(Hobbie(name: key, category: values[1], thumbnail: values[0]));
    });
    return hobbies;
  }

  List<String> toValueList(Map jsonMap) {
    List<String> values = List.empty(growable: true);
    jsonMap.forEach((key, value) {
      values.add("$value");
    });
    return values;
  }

  Future<void> selectHobbie(Hobbie hobbie, String username) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return;
    selectedHobbies.add(hobbie);
    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("hobbies/${hobbie.name}/users");
    dbRef.update({username: true});
    notifyListeners();
  }

  Future<void> deselectHobbie(Hobbie hobbie, String username) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return;
    var item =
        selectedHobbies.firstWhere((element) => element.name == hobbie.name);
    selectedHobbies.remove(item);
    final DatabaseReference dbRef =
        FirebaseDatabase.instance.ref("hobbies/${hobbie.name}/users/$username");
    dbRef.remove();
    notifyListeners();
  }

  bool isHobbieSelected(List<Hobbie> list, Hobbie hobbie) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].name == hobbie.name) return true;
    }
    return false;
  }

  Future<void> setSelectedHobbies(List<String>? hobbiesNames) async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) return;
    selectedHobbies = List.empty(growable: true);
    if (hobbiesNames == null) return;
    for (var i = 0; i < hobbiesNames.length; i++) {
      Hobbie? hob = await getHobbieByName(hobbiesNames[i]);
      if (hob != null) selectedHobbies.add(hob);
    }
  }

  void setLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  void checkConnection() async {
    hasConnection = await InternetConnectionChecker().hasConnection;
  }
}
