import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthService extends ChangeNotifier {
  final String _baseUrl = "identitytoolkit.googleapis.com";
  final String _firebaseToken = "AIzaSyCBPE9HdFvbUkIF7R1T20WaxkKciYQ1ZgI";

  final storage = const FlutterSecureStorage();

  Future<String?> createUser(String email, String password) async {
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final url =
        Uri.https(_baseUrl, '/v1/accounts:signUp', {'key': _firebaseToken});

    final resp = await http.post(url, body: json.encode(authData));
    final Map<String, dynamic> decodedResp = jsonDecode(resp.body);

    if (decodedResp.containsKey("idToken")) {
      await storage.write(key: "token", value: decodedResp["idToken"]);
      await storage.write(key: "email", value: email);
      await storage.write(key: "password", value: password);
      return null;
    } else {
      return decodedResp["error"]["message"];
    }
  }

  Future<String?> login(String email, String password) async {
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final url = Uri.https(
        _baseUrl, '/v1/accounts:signInWithPassword', {'key': _firebaseToken});

    final resp = await http.post(url, body: json.encode(authData));
    final Map<String, dynamic> decodedResp = jsonDecode(resp.body);

    if (decodedResp.containsKey("idToken")) {
      await storage.write(key: "token", value: decodedResp["idToken"]);
      await storage.write(key: "email", value: email);
      await storage.write(key: "password", value: password);
      return null;
    } else {
      return decodedResp["error"]["message"];
    }
  }

  Future logout() async {
    await storage.delete(key: "token");
    await storage.delete(key: "email");
    await storage.delete(key: "password");
  }

  Future<String> readSavedEmail() async {
    return await storage.read(key: "email") ?? "";
  }

  void resetPassword(String email) {
    final auth = FirebaseAuth.instance;
    auth.sendPasswordResetEmail(email: email);
  }
}
