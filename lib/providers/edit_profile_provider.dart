import 'package:flutter/material.dart';
import 'package:homeet/models/models.dart';

class EditProfileProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey();

  User user;

  EditProfileProvider(this.user);

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }

  updateGender(String value) {
    user.gender = value;
    notifyListeners();
  }

  updateBirthDate(DateTime value) {
    user.birthDate = value;
    notifyListeners();
  }
}
