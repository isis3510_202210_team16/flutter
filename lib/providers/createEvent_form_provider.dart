import 'package:flutter/material.dart';

class EventCreateformProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey();

  String name = "";
  String location = "";
  DateTime date = DateTime.now();
  String price = "";
  String id = "";

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }

  updateName(String value) {
    name = value;
    notifyListeners();
  }

  updateLocation(String value) {
    location = value;
    notifyListeners();
  }

  updateDate(DateTime value) {
    date = value;
    notifyListeners();
  }

  updatePrice(String value) {
    price = value;
    notifyListeners();
  }

  updateId(String value) {
    id = value;
    notifyListeners();
  }
}
